# Final_Assessment_ProkopevaEG
Итоговый проект слушателя курса повышения квалификации "Аналитик данных (Data Scientist)" ЦДО МГТУ им. Баумана.
Группа: 12824 АНДАН
ФИО слушателя: Прокопьева Елена Георгиевна
Тема проекта: «Прогнозирование размеров сварного шва при электронно-лучевой сварке
тонкостенных конструкций аэрокосмического назначения»
В результате выполнения данного проекта было реализовано веб-приложение, являющееся оболочкой для выбранной из нескольких обученных моделей машинного обучения. Целью приложения является прогнозирование ширины и глубины сварного
соединения.
