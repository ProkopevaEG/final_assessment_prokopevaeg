# Блок импорта библиотек
# import numpy as np
from flask import Flask, render_template, request
import pickle

app = Flask(__name__)

# Загрузка ML-модели и определение ее как функции
def model(params):
    with open("B:\Учебные материалы\Final_assessment\Final_ProkopevaEG\RandomForestModel.pkl", "rb") as f:
        model = pickle.load(f)
        y_pred = model.predict(params)
        return (f"Глубина шва Depth: {round(y_pred[0][0], 3)} "
                f"Ширина шва: {round(y_pred[0][1], 3)}")

# Сбор входных параметров из html-страницы приложения
@app.route('/', methods=['get', 'post'])
def main():
    #params = [[]]
    message = ''
    if request.method == "POST":
        IW = float(request.form.get("IW"))
        IF = float(request.form.get("IF"))
        VW = float(request.form.get("VW"))
        FP = float(request.form.get("FP"))

        params = [[IW, IF, VW, FP]]

        message = model(params)
    return render_template('index.html', message=message)

# Запуск приложения
# app.run()
